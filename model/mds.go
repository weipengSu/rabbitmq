package model

import (
	"github.com/rabbitmq/amqp091-go"
)

type RabbitMq struct {
	// 连接
	conn *amqp091.Connection

	//管道
	channel *amqp091.Channel

	// 队列名称
	QueueName string

	// 交换机
	Exchange string

	// key simple 模式, 几乎用不到
	Key string

	// 链接信息
	MQURL string
}
