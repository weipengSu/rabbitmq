package model

import (
	"fmt"
	"github.com/rabbitmq/amqp091-go"
)

func (r *RabbitMq) PublishSimple(message string) {
	// 申请队列，如果队列存在就跳过，不存在则创建
	// 优点，保证队列存在，消息能发送到队列中
	_, err := r.channel.QueueDeclare(
		//队列名称
		r.QueueName,
		//是否持久化
		false,
		//是否为自动删除 当最后一个消费者断开连接之后，是否把消息从队列中删除
		false,
		//是否具有排他性 true表示自己可见 其他用户不能访问
		false,
		//是否阻塞 true表示要等待服务器的响应
		false,
		//额外数学系
		nil)

	if err != nil {
		fmt.Println(err)
	}

	// 发送消息到队列中
	r.channel.Publish(
		//默认的Exchange交换机是default,类型是direct直接类型
		r.Exchange,
		//要赋值的队列名称
		r.QueueName,
		//如果为true，根据exchange类型和routekey规则，如果无法找到符合条件的队列那么会把发送的消息返回给发送者
		false,
		//如果为true,当exchange发送消息到队列后发现队列上没有绑定消费者，则会把消息还给发送者
		false,
		//消息
		amqp091.Publishing{
			//类型
			ContentType: "text/plain",
			//消息
			Body: []byte(message),
		})

}

// https://www.cnblogs.com/linyue09/p/14156046.html
