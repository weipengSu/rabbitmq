package model

import (
	"fmt"
	"github.com/rabbitmq/amqp091-go"
	"log"
)

const MQURL = "amqp://guest:guest@192.168.123.130:5672/"

func NewRabbitMQ(queuename string, exchange string, key string) *RabbitMq {
	rabbitmq := &RabbitMq{QueueName: queuename, Exchange: exchange, Key: key, MQURL: MQURL}
	var err error

	rabbitmq.conn, err = amqp091.Dial(rabbitmq.MQURL)
	rabbitmq.failOnErr(err, "创建连接错误！")

	rabbitmq.channel, err = rabbitmq.conn.Channel()
	rabbitmq.failOnErr(err, "获取channel失败")
	return rabbitmq
}

//错误处理函数
func (r *RabbitMq) failOnErr(err error, message string) {
	if err != nil {
		log.Fatalf("%s:%s", message, err)
		panic(fmt.Sprintf("%s:%s", message, err))
	}
}

// 断开channel 和 connection

func (r *RabbitMq) Destory() {
	r.channel.Close()
	r.conn.Close()
}

// 简单模式创建mq 实例

func NewRabbitMQSimple(queueName string) *RabbitMq {
	return NewRabbitMQ(queueName, "", "")
}
