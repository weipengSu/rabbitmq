package main

import (
	"fmt"
	"rabbitmq/model"
)

func main() {
	rabbitmq := model.NewRabbitMQ("imoocSimple", "", "")
	for i := 0; i < 10; i++ {
		s := fmt.Sprintf("%d, %s", i, "+ hello mooc")
		rabbitmq.PublishSimple(s)
	}

	rabbitmq.ConsumeSimple()
}
